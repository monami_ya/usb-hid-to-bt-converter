/* Copyright (C) 2004 Monami-ya LLC, Japan.
 * Licensed under GPL3 or above.
 */
#include <stdio.h>
#include <usb.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>

static int
open_bt_hid()
{
  int fd;
  struct termios params = { 0 };

  fd = open("/dev/ttyUSB1", O_RDWR | O_NOCTTY | O_NDELAY);
  if (fd == -1) {
    goto raise;
  }

  if (!isatty(fd)) {
    goto close_raise;
  }

  /* blocking read */
  fcntl(fd, F_SETFL, 0);

  params.c_cflag |= B115200;
  params.c_cflag |= CLOCAL;
  params.c_cflag |= CREAD;

  params.c_cflag &= ~ECHO;
  params.c_cflag &= ~ECHOE;

  // set to 8N1
  params.c_cflag &= ~PARENB;
  params.c_cflag &= ~CSTOPB;
  params.c_cflag &= ~CSIZE;
  params.c_cflag |= CS8;

  params.c_oflag = 0;

  if (tcsetattr(fd, TCSANOW, &params) == -1) {
    goto close_raise;
  }

  return fd;

 close_raise:
  close(fd);
 raise:
  return -1;
}

static int
convert(struct usb_device *device)
{
  int fd;
  usb_dev_handle *handle;
  struct usb_config_descriptor *config = &device->config[0];
  struct usb_interface *interface = &config->interface[0];
  struct usb_interface_descriptor *altsetting = &interface->altsetting[0];
  struct usb_endpoint_descriptor *endpoint = &altsetting->endpoint[0];
  uint8_t ep = endpoint->bEndpointAddress;

  unsigned char buf[4096];

  fd = open_bt_hid();
  if (fd == -1) {
    fprintf(stderr, "Failed to open BT adapter.");
    return -1;
  }

  handle = usb_open(device);
  usb_get_string_simple(handle,
			device->descriptor.iProduct,
			(char *)buf, sizeof(buf));

  printf("USB-DEV: 0x%04x/0x%04x \"%s\"\n",
	 device->descriptor.idVendor, device->descriptor.idProduct, buf);

  if (usb_set_configuration(handle, config->bConfigurationValue) < 0) {
    if (usb_detach_kernel_driver_np(handle,
				    altsetting->bInterfaceNumber) < 0) {
      fprintf(stderr, "error in usb_set_configuration().\n");
      usb_close(handle);
      return -1;
    }
  }

  usb_claim_interface(handle, altsetting->bInterfaceNumber);

  while (1) {
    ssize_t read_size;
    unsigned char packet[11] = { 0xfd, 0x09, 0x01, 0 };
    size_t i;
    int ret;

    read_size = usb_interrupt_read(handle, ep, (char *)buf,
				   endpoint->wMaxPacketSize, 1000);
    if (read_size < 0) {
      if (read_size != -ETIMEDOUT) {
	fprintf(stderr, "read error: %d\n", read_size);
      }
    } else {
      for (i = 0; i < 8; i++) {
	packet[i + 3] = buf[i];
      }
      packet[4] = 0;

      ret = write(fd, (char *)packet, sizeof(packet));
      if (ret < 0) {
	fprintf(stderr, "write error.");
      }

      for (i = 0; i < sizeof(packet); i++) {
	printf(":%02x", packet[i]);
      }
      printf(":\n");
    }
  }

  usb_resetep(handle, ep);
  usb_release_interface(handle, altsetting->bInterfaceNumber);

  usb_close(handle);
  return 0;
}

static int
check_keyboard(struct usb_device *device)
{
  struct usb_config_descriptor *config = &device->config[0];
  struct usb_interface *interface = &config->interface[0];
  struct usb_interface_descriptor *altsetting = &interface->altsetting[0];
 
  return altsetting->bInterfaceClass == 3 &&
    altsetting->bInterfaceSubClass == 1 &&
    altsetting->bInterfaceProtocol == 1;
}

int main(void)
{
  struct usb_bus *bus;
  struct usb_device *device;

  usb_init();

  if (!usb_get_busses()) {
    usb_find_busses();
    usb_find_devices();
  }

  for (bus = usb_get_busses(); bus; bus = bus->next) {
    for (device = bus->devices; device; device = device->next) {
      if (device->descriptor.idVendor != 0 ||
	  device->descriptor.idProduct != 0) {
	if (check_keyboard(device)) {
	  convert(device);
	  return 0;
	}
      }
    }
  }
  return 0;
}
